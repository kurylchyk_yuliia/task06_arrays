package com.custom_deque;
import java.util.Deque;

public class Application {
    public static void main(String[] args) {
        Integer[] arr = new Integer[10];
        MyDeque<Integer> d  = new MyDeque<Integer>(arr);

        d.add(12); //to tail
        d.add(13); //to tail
        d.add(14); //to tail
        d.removeFirst(); //removing 12 from head
       d.offerFirst(15); //setting 15 into head
       d.offerLast(22); // setting 22 to tail
       d.removeFirst(); // removing 15;
        d.addLast(45); //setting 45 to tail
        System.out.println(d.peek());
        System.out.println(d.peekLast());
        System.out.println(d.peekFirst());
    }
}
