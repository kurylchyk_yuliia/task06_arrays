package com.custom_deque;
import org.apache.logging.log4j.*;


/**
 * class MyDeque that shows the common work of real Deque
 *
 * @param <T>
 */


public class MyDeque<T>  {

    /**
     * value for logger
     */
    private static Logger logger1 = LogManager.getLogger(MyDeque.class);
    /**
     * array which will contain the elements
     */
    private T[] deque;
    /**
     * index of first element
     */
    private int indexOfHead;
    /**
     * index of the last added element
     */
    private int indexOfTail;

    /**
     * Initialize the values
     * @param arr
     */
    MyDeque(T[] arr) {
        deque = arr;
        indexOfHead = indexOfTail = 0;
    }

    /**
     * Adds an element to the tail
     * @param t
     * @return true if the operation was successful
     */
    public boolean offer(T t) {
        if (indexOfTail != deque.length) {
            deque[indexOfTail] = t;
            indexOfTail++;
            return true;
        } else return false;
    }

    /**
     * Adds an element to the tail.
     * @param t
     * @return true if operation was successful
     */

    public boolean add(T t) {

        if (indexOfTail != deque.length) {
            deque[indexOfTail] = t;
            indexOfTail++;
            return true;
        } else {
            throw  new IllegalStateException();
        }
    }


    /**
     * Adds an element to the head.
     * @param t
     */
    public void addFirst(T t) {
        if (indexOfTail != deque.length) {
            for (int index = 0; index < deque.length; index++) {
                deque[index + 1] = deque[index];
            }
            indexOfTail++;
            deque[indexOfHead] = t;
        } else {
            logger1.error("Sorry! Can not add the element to the head of deque");
        }

    }

    /**
     * Adds an element to the tail.
     * @param t
     */
    public void addLast(T t) {
        add(t);
    }

    /**
     * Adds an element to the head.
     * @param t
     */
    public void push(T t) {
        addFirst(t);
    }

    /**
     * Removes the element at the head.
     * @return the element which was removed or null
     */
    public T removeFirst() {
        if(!isEmpty()) {
            T temp = deque[indexOfHead];

            for(int index = 0; index<indexOfTail; index++) {
                deque[index] = deque[index+1];
            }
            indexOfTail--;
            return temp;
        } else {
            logger1.warn("The deque is empty");
            return null;
        }

    }

    /**
     * Check if the container is empty
     * @return true if it is empty
     */
    public boolean isEmpty() {
        if(((indexOfTail==0&&indexOfHead==0) && deque[indexOfHead]==null)) {
            return true;
        } else return false;

    }

    /**
     *
     * @return the size of container
     */
    public int length() {
        return deque.length;
    }

    /**
     * Removes the element at the tail.
     * @return remover element or null
     */
    public T removeLast() {
        if(!isEmpty()) {
            T temp = deque[indexOfTail];
            deque[indexOfTail]=null;
            indexOfTail--;
            return deque[indexOfTail];
        } else {
            logger1.warn("The deque  is empty!");
            return null;
        }
    }

    /**
     *  Adds an element to the head
     * @param t
     * @return true if element was added
     */
    public boolean offerFirst(T t) {
        if (indexOfTail != deque.length) {
            for (int index = 0 ; index < deque.length-1; index++) {
                deque[index +1] = deque[index];
            }
            indexOfTail++;
            deque[indexOfHead] = t;
            return true;
        } else {
            logger1.error("Sorry! Can not add the element to the head of deque");
            return false;
        }
    }

    /**
     * Adds an element to the tail
     * @param t
     * @return true if element was added
     */
    public boolean offerLast(T t) {
        return offer(t);
    }

    /**
     * shows the first element
     * @return the first element
     */
    public T peek() {
        if(!isEmpty()) {
            return deque[indexOfHead];
        } else {
            return null;
        }
    }

    /**
     * shows the first element
     * @return the first element
     */
    public T peekFirst() {
        return peek();
    }

    /**
     * shows the last element
     * @return the last element
     */
    public T peekLast() {
        if(!isEmpty()) {
            return deque[indexOfTail-1];
        } else {
            return null;
        }
    }

}