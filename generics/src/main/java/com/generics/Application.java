package com.generics;
import org.apache.logging.log4j.*;

/*
 * Class Application which show info.
 */
public class Application {
    /*
     * Logger - obj which works with log4j
     */
    private static Logger logger1 = LogManager.getLogger(Application.class);

    /*
     * method main
     * @param args
     */
    public static void main(String[] args) {

        MyGen<Integer> iGen = new MyGen<Integer>(15);
        logger1.trace(iGen);
        logger1.trace(iGen.getGen());

        iGen.setGen(new Integer(18));
        logger1.trace(iGen.getGen());

        MyGen<Double> dGen = new MyGen<Double>(1.7);
        MyGen<Integer> iGen2 = new MyGen<Integer>(1);

        if(iGen.compare(dGen)){
            logger1.info("They are equal");
        } else {
        logger1.error("They are not equal");
        }

        logger1.info("Value = " + iGen.add(iGen2));
        logger1.info("Value = " + iGen.sub(iGen2));


        logger1.info(MyGen.showNameOfType(new Double(1.3)));
        logger1.info(MyGen.showNameOfType(new Character('j')));
        logger1.info(MyGen.showNameOfType(new String("hello")));
        logger1.info(MyGen.showNameOfType(new Boolean(true)));
    }
}
