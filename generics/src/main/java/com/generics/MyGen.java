package com.generics;

/**
 * class MyGen.
 * It warks with generics type which extends Number
 * It means that it is not able to work with String for example
 * @param <T>
 */
public class MyGen<T extends Number> {
    T num;

    /**
     * Constructor.
     * Set value to
     * see @num
     * @param ob
     */
  public   MyGen(T ob) {
        this.num  = ob;
    }

    /**
     * Method which add 2 numbers of general type
     * @param ob
     * @return double value of adding 2 numbers
     */
    public double add(MyGen<? extends T> ob){

      double value = this.num.doubleValue()+ob.num.doubleValue();
       return value;
    }

    /**
     *
     * @param ob
     * @return double value of substracting of 2 numbers
     */
  public double sub(MyGen<? extends T> ob){

    double value = this.num.doubleValue()-ob.num.doubleValue();
    return value;
  }


    /**
     * Compare 2 objects
     * @param ob
     * @return
     */
  public  boolean compare(MyGen<?> ob) {
        if(this.num.doubleValue() == ob.num.doubleValue()) {
          return true;
        } else  {
          return false;
        }


    }


    /**
     *
     * @return number of an object
     */
    public String getGen() {
      return num.toString();
    }


    /**
     * set new value to an object
     * @param ob
     */
    public void setGen(T ob) {

       this.num = ob;
    }

    /**
     * shows the type of object
     * @param type
     * @param <T>
     * @return
     */

    public static<T> String showNameOfType(T type) {
        return (type.getClass().getName());
    }

}
