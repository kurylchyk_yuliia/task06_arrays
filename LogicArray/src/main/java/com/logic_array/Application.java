package com.logic_array;

import org.apache.logging.log4j.*;

public class Application {

    private static Logger logger1 = LogManager.getLogger(Application.class);

    static<T extends Number & Comparable<T>> void showArray(T[] arr) {
        int index = 0;
        logger1.trace(arr.getClass().getName());
        while (arr[index]!=null) {
            logger1.trace (arr[index] + "\t");
            index++;
        }
        logger1.trace("\n");
    }



    public static void main(String[] args) {


        Integer[] arr1 = {1, 2, 3, 5, 7, 6};
        Integer[] arr2 = {1, 2, 6, 9, 0, 10, 13, 16};
        MyArray myArray = new MyArray(arr1, arr2);

        Integer[] arr3 = new Integer[10];
        Integer[] arr4 = new Integer[15];

        myArray.getJointArray(arr3);

        showArray(arr3);

        myArray.getDifferentArray(arr4);

        showArray(arr4);

        Integer[] arr5 =new Integer[]{5,3,7,0,3,9,3,7};
        MyArray.check(arr5);
        showArray(arr5);

        Integer[] arr6 =new Integer[]{5,5,3,5,3,3,3,4,7};
        MyArray.deleteButLeaveOnly1(arr6);
        showArray(arr6);

    }

}
