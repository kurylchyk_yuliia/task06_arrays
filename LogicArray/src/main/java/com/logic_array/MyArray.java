package com.logic_array;

/**
 * class MyArray with generics
 * @param <T>
 */
public class MyArray<T extends Number> {

    /**
     * array of general type
     */
    T[] firstArray;
    /**
     * array of general type
     */
    T[] secondArray;

    /**
     * setting the values
     * @param first
     * @param second
     */

    MyArray(T[] first, T[] second) {
        firstArray = first;
        secondArray = second;
    }


    /**
     * creates an array that consist of join elements of two array
     * @param third array with witch the operations will be made
     */
    public void getJointArray(T[] third) {


        int k = 0;

        for (int index = 0; index < firstArray.length; index++) {
            for (int jdex = 0; jdex < secondArray.length; jdex++) {
                if (firstArray[index] == secondArray[jdex]) {
                    third[k] = firstArray[index];
                    k++;
                }
            }
        }

    }


    /**
     * creates an array which will have element from array which are not the same in 2 arrays
     * @param third array with witch the operations will be made
     */
    public void getDifferentArray(T[] third) {
        for(int index = 0; index< firstArray.length; index++) {
                for (int jdex = 0; jdex < secondArray.length; jdex++)
                    if (firstArray[index] == secondArray[jdex]) {
                        firstArray[index] = null;
                        secondArray[jdex] = null;
                    }
        }


        int k = 0;

        for(int index = 0; index< firstArray.length; index++)
        {
            if(firstArray[index]!=null) {
                third[k] = firstArray[index];
                k++;
            }else continue;
        }
        for(int index = 0;index<secondArray.length; index++){
            if(secondArray[index]!=null) {
                third[k] = secondArray[index];
                k++;
            } else continue;
        }
    }

    /**
     * checks if in array there are more than two same elements
     * @param arr to search in
     * @param <T>
     */
    public static<T extends Number &Comparable<T>> void check(T[] arr) {

        sort(arr);
        T element;

        int reduceTheSizeOfIteration = 0;
        int hasOnly2 = 1;
        for(int index = 0; index <arr.length - reduceTheSizeOfIteration;index++) {
            element = arr[index];
            for(int jdex = index+1; jdex<arr.length - reduceTheSizeOfIteration; jdex++)
            {
                if(arr[index].compareTo(arr[jdex])==0) {
                    hasOnly2++;
                }
            }

            if(hasOnly2>2) {

                hasOnly2=1;
             reduceTheSizeOfIteration+=  deleteElement(arr,arr[index]);

            }
        }

    }


    /**
     * deletes same elements
     * @param arr to do manipulation
     * @param element the value of repeted element
     * @param <T>
     * @return how much element it has deletad
     */
    private static <T extends  Number & Comparable<T>>  int deleteElement(T[] arr, T element) {

        int howMuch = 0;
        for(int index = 0; index<arr.length; index++) {
            if(arr[index].compareTo(element)==0) {
                arr[index]=null;
                howMuch++;
            }
        }

        for(int index = 0; index< arr.length-howMuch; index++) {

            if(arr[index]==null) {
                arr[index] = arr[index+howMuch];
                arr[index+howMuch] = null;
            }
        }

        return howMuch;

    }

    /**
     * sorts the array
     * @param arr
     * @param <T>
     */
    public static<T extends Number & Comparable<T>> void sort(T[] arr) {

        for(int index = 0; index<arr.length; index++) {
            for(int jdex = index+1; jdex<arr.length; jdex++) {

                if(arr[index].compareTo(arr[jdex])>0) {
                        T temp = arr[index];
                        arr[index] = arr[jdex];
                        arr[jdex] = temp;
                } else continue;
            }
        }
    }

    /**
     * deletes repeted elements if they go ine after another but leave the first one
     * @param arr
     * @param <T>
     */

    public static <T extends Number &Comparable<T>> void  deleteButLeaveOnly1(T[] arr) {


        for(int index = 0 ; index< arr.length-1; index++) {


            if(arr[index] == arr[index+1]) {
                int jdex = index+1;
                while(arr[jdex]==arr[index]) {
                    arr[jdex] = null;
                    jdex++;
                }
            }
        }

        for(int index = 0; index<arr.length; index++) {
            if(arr[index]==null) {

                for(int jdex = index+1; jdex<arr.length;jdex++) {
                    if(arr[jdex]!=null) {
                        arr[index] = arr[jdex];
                        arr[jdex] = null;
                        break;
                    }else continue;

                }
            }
        }

    }



}

