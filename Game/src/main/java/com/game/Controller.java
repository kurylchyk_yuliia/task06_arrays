package com.game;
import java.util.Scanner;

/**
 * Class Controller.
 * Operates with @see::View
 * and see@CircleHall;
 */
public class Controller {

    /**
     * Object of class CircleHall
     */
    CircleHall circleHall = new CircleHall();
    /**
     * Object of class Hero
     */
    Hero hero = new Hero();
    /**
     * Object of class View
     */
    View view = new View();


    /**
     * Constructor of class Contoller
     * will repeat all the methods while hero is alive or there are no  monster there
     */
    Controller() {

        while (hero.powerOfHero > 0 && circleHall.getCountOfMonstersBehindDoor() > 0) {
            int answer = view.menu(getMenu());
            check(answer);
            getInfoAboutHero();
        }

    }


    /**
     * Represent the menu
     * @return all the menu
     */
    public String getMenu() {

        return "1 - See the tips\n2 -" +
                " get suggestion about which door will not " +
                "lead to death\n3 - Open the next door\n4 - find out how many monsters are behind doors";


    }


    /**
     * Allow to choose appropriate method according to the answer given by user
     * @param answer given by user
     */

    public void check(int answer) {
        Scanner sc = new Scanner(System.in);

        switch (answer) {
            case 1:
                circleHall.getTooltip();
                break;
            case 2:
                circleHall.getSuggestion(hero);
                break;
            case 3:
                int numberOfDoor = circleHall.chooseTheDoor();
                view.showInfo("Open the door number" + numberOfDoor + "?yes/no");
                String a = sc.nextLine();
                if (a != "no" || a != "n") {
                    circleHall.openTheDoor(hero, numberOfDoor);
                } else {
                    view.showInfo("Ok");
                }
                break;

            case 4:
                view.showInfo("There are " + circleHall.getCountOfMonstersBehindDoor() + "doors with monsters");
                break;
        }

    }

    /**
     * Show all the information about hero and checks if he is alive
     * or if there are any monster left
     */

    public void getInfoAboutHero() {
        if (hero.powerOfHero <= 0) {
            view.showInfo("Game over");
            System.exit(0);
        } else if (circleHall.getCountOfMonstersBehindDoor() == 0) {
            view.showInfo("There are no monster left! You win");
            System.exit(0);
        } else {
            view.showInfo("You now have " + hero.getPowerOfHero() + " of power");
        }

    }
}
