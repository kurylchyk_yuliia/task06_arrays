package com.game;
import java.util.Scanner;

/**
 * class View used for showing the information
 */
public class View {
    /**
     * constructor
     */
    View () {


    }

    /**
     * represent the menu and gets the answer
     * @param menu
     * @return
     */
    public int menu(String menu) {
        int answer = 0;
         do{
            System.out.println("Enter the option from 1 to 4");
            Scanner sc = new Scanner(System.in);
            System.out.println(menu);
            answer = sc.nextInt();
        }while(answer<1&&answer>4);

        return  answer;
    }

    /**
     * show different kind of information
     * @param info
     */
    public void showInfo(String info) {

        System.out.println(info);
    }
}
