package com.game;

/**
 * interface BehindDoor
 */
public interface BehindDoor {

    /**
     *
     * @return the power
     */
     int getPower();

    /**
     * monipulates with Hero
     * @param h
     */
     void manipulationWithPower(Hero h);


}
