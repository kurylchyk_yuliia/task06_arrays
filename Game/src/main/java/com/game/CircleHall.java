package com.game;
import java.util.Random;
import java.util.Scanner;

/**
 * Class CircleHall
 * that allow you to "open the door" and fight with monsters
 * or get an artifact
 */

public class CircleHall {

    /**
     * random for gettig random value
     */
    Random random = new Random();
    /**
     * reference to object of BehindDoors
     */
    BehindDoor[] behindDoors;

    /**
     * variable that shows the count of monsters
     *
     */
    static int monsterBehindDoor = 0;

    /**
     * Constructor initialize all the variable and set the value to array behindDoors
     * @link behindDoors
     *
     */
    CircleHall() {
        behindDoors = new BehindDoor[10];
        int rand;
        monsterBehindDoor = 0;

        for (int index = 0; index < 10; index++) {
            rand = random.nextInt(2)+1;
            switch (rand) {

                case 0:
                case 1:
                    behindDoors[index] = new Artifact();
                    break;
                case 2:
                    behindDoors[index] = new Monster();
                    monsterBehindDoor++;
                    break;
            }
        }


    }


    /**
     * Allow ti choose the number of door
     * @return number of door
     */
    public int chooseTheDoor(){
        int answer;
        System.out.println("Choose the door from 0 to 9");
        Scanner sc  = new Scanner(System.in);
        answer = sc.nextInt();

        if(answer>10 || answer<0) {
            answer=0;
        }
        return answer;
    }


    /**
     * Represent the tooltip where is a monster or artifact
     */
    public void getTooltip() {
        for(int index = 0; index<behindDoors.length; index++) {

            System.out.println(index + "\t"
                    + behindDoors[index].getClass().getName()
                    + "\t" + behindDoors[index].getPower());
        }

    }

    /**
     * for appropriate door chosen by user
     * calls method  @link Monster::manipulateWithPower
     * or @link Artifact::manipulateWithPower
     *
     * @param h - the reference of object Hero
     * @param index of chosen door
     */

    public void openTheDoor(Hero h,int index) {
        behindDoors[index].manipulationWithPower(h);
    }

    /**
     *
     * @return the count of monster
     */
    public int getCountOfMonstersBehindDoor() {
        return monsterBehindDoor;

    }

    /**
     * show suggestion how can you open the door to survive
     * @param h
     */
    public void getSuggestion(Hero h) {
        System.out.print("You can choose ");
        for(int index = 0; index<behindDoors.length; index++) {

            if(h.getPowerOfHero()>behindDoors[index].getPower() && behindDoors[index].getPower()!=0){
                System.out.println( index + "door");
            }
        }

    }
}
