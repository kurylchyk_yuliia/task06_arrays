package com.game;
import java.util.Random;

/**
 * class Monster implements @see BehindDoor
 */
public class Monster implements BehindDoor{
    /**
     * value of power
     */
    int powerOfMonster;
    /**
     * for generation random value
     */
    Random random;

    /**
     * set the variables' values
     */
    Monster () {
        random = new Random();
        powerOfMonster = random.nextInt(100-5)+5;
    }

    /**
     *
     * @return power of Monster
     */
    public int getPower() {
        return powerOfMonster;
    }

    /**
     * manipulates with hero's power
     * @param h - the reference of object Hero
     */
    public void manipulationWithPower(Hero h) {
        if(powerOfMonster==0) {
            System.out.println("Monster was killed!");
            return;
        }
        System.out.println("You are fighting with monster");
        System.out.println(" - " + powerOfMonster);
        h.powerOfHero-=powerOfMonster;
        powerOfMonster = 0;
        CircleHall.monsterBehindDoor--;
    }
}
