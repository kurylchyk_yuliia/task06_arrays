package com.game;
import java.util.Random;

/**
 * class Artifact  implements
 * @link BehindDoor interface
 *
 */
public class Artifact implements BehindDoor{
    /**
     * Valiable random for generating random values
     */
    Random random;
    /**
     * value which represent the value of power
     */
    int powerOfArtifact;


    /**
     * Constructor of Artifact
     * Initialize the values
     */
    Artifact () {
        random = new Random();
        powerOfArtifact = random.nextInt(80-10)+10;
    }

    /**
     *
     * @return the value of power
     */
    public int getPower() {
        return powerOfArtifact;
    }

    /**
     * manipulates with hero's power
     * @param h - the reference of object Hero
     */
    public void manipulationWithPower(Hero h) {
        if(powerOfArtifact==0) {
            System.out.println("There is no left");
            return;
        }
        System.out.println("You are getting more power");
        System.out.println(" + " + powerOfArtifact);
        h.powerOfHero+=powerOfArtifact;
        powerOfArtifact = 0;
    }
}
