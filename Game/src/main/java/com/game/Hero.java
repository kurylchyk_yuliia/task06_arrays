package com.game;

/**
 * class Hero
 */
public class Hero {

    /**
     * value of hero's power
     */
    protected int powerOfHero;

    /**
     * sets hero's power
     */
    Hero() {
        powerOfHero = 25;

    }

    /**
     *
     * @return hero's power
      */
    public int getPowerOfHero() {
        return powerOfHero;
    }
}
