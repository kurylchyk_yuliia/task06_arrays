package com.array_and_string;
import java.util.Arrays;
import java.util.Collections;
import java.util.ArrayList;

public class Application {
    public static void main(String[] args) {

        ArrayOfStr arrayOfStr = new ArrayOfStr();
        arrayOfStr.add("Hardin");
        arrayOfStr.add("Tessa");
        arrayOfStr.add("Molly");
        arrayOfStr.add("Steph");
        arrayOfStr.add("Zed");
        arrayOfStr.add("Noah");
        arrayOfStr.add("Jill");
        arrayOfStr.add("Ben");
        System.out.println("ArrayOfStr has" + arrayOfStr.length() + " elements");
        System.out.println("ArrayOfStr capacity" + arrayOfStr.capacity());

        for(int index = 0; index<arrayOfStr.length(); index++) {

            System.out.print(arrayOfStr.get(index) + "\t");
        }

        ArrayList<String> arrayList = new ArrayList<String>();

        arrayList.add("Robert");
        arrayList.add("Alice");
        arrayList.add("Kate");
        arrayList.add("Sam");
        arrayList.add("Dean");
        arrayList.add("Bob");
        arrayList.add("Theo");
        arrayList.add("Misha");

        System.out.println("\nArrayList has" + arrayList.size() + " elements");

        for(int index = 0; index<arrayOfStr.length(); index++) {

            System.out.print(arrayList.get(index) + "\t");
        }



        /**
         * Next line is not correct because ArrayList does not represent the capacity
         * You can only use the constuctor if you want to set the capacity while creating arrayList
         * of use method ensureCapacity()
         */
        //System.out.println("ArrayList capacity" + arrayList.capacity());

        //--------------------------Array-----------------------------------------------------
        System.out.println("\n");
        Container container = new Container();

        Comparable<String> arrSort[] = new String[5];
        for(int index = 0; index<arrSort.length; index++) {
            arrSort[index] = container.getSort();
        }

        for(int index = 0; index< arrSort.length; index++) {

            System.out.print(arrSort[index]+"\t");
        }

        System.out.println("\n");
        Arrays.sort(arrSort);
        for(int index = 0; index< arrSort.length; index++) {

            System.out.print(arrSort[index]+"\t");
        }
        //sort is working

       if(Arrays.binarySearch(arrSort,"Toyota")<0) {
           System.out.println("\nThere is no sort of car named Toyota");
       } else {
           System.out.println("\nThe index of Toyota is " +Arrays.binarySearch(arrSort,"Toyota"));
       }

        //-------------ArrayList<String>
        ArrayList<String> arrModel = new ArrayList<String>(5);
          for(int index = 0; index<5; index++) {
              arrModel.add(container.getModel());

          }
        System.out.println("");
        for(int index = 0; index<arrModel.size(); index++) {
            System.out.print( arrModel.get(index) +"\t");
        }

       Collections.sort(arrModel);

        //sort works
        System.out.println("\n");
        for(int index = 0; index<arrModel.size(); index++) {
            System.out.print(arrModel.get(index) +"\t");
        }

        if(Collections.binarySearch(arrModel,"X6")<0) {
            System.out.println("\nThere is no model of car named X6");
        } else {
            System.out.println("\nThe index of element X6 = " + Collections.binarySearch(arrModel,"X6"));
        }




    }
}
