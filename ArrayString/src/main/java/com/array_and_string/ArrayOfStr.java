package com.array_and_string;

public class ArrayOfStr {

   private String arr[];
   private int indexOfAdding;

    ArrayOfStr () {
        arr = new String[10];
        indexOfAdding=0;
    }

    public void add(String str) {
        if(indexOfAdding>=arr.length) {
            resize();
        }

        arr[indexOfAdding] = str;
        indexOfAdding++;

    }
    public String get(int index)  throws ArrayIndexOutOfBoundsException{
        return arr[index];
    }

    private void resize() {
        String[] temp = new String[arr.length*2];

        for(int index = 0; index< arr.length; index++) {
            temp[index] = arr[index];
        }
        arr = temp;
    }

    /**
     * not real length, but length of added element
     * @return
     */
    public int length() {

        return indexOfAdding;
    }

    /**
     * real lenght of array
     * @return
     */

    public int capacity(){
        return  arr.length;
    }

}
