package com.array_and_string;
import java.util.Random;

public class Container  {

   private Comparable<String> obOfSort;
   private String obOfModel;
   private String[] sort = {"Honda","Toyota","Mercedes-Benz","BMW","Ford","Peugeot","Renault"};
   private String[] model = {"HR-V","Rav-4","E-350", "X6","Mustang","208","Logan"};
   private Random random;

    Container() {
        random = new Random();
    }

    private void generateSortOfCar() {
        int index = random.nextInt(sort.length);
       obOfSort = sort[index];

    }

    private void generateModelOfCar() {
        int index = random.nextInt(model.length);
        obOfModel = model[index];
    }

    public Comparable<String> getSort() {
        generateSortOfCar();
        return obOfSort;
    }

    public String getModel() {
        generateModelOfCar();
        return obOfModel;
    }

}
