package com.priority_queue;

/**
 * PriorityQueue without implementation Queue.
 * Just my own
 */

public class PriorityQueue<T extends Number & Comparable <T>> {
    /**
     * array for storing element.
     */
    private T[] queue;
    /**
     * the index for removing
     */
    private int putlog;
    /**
     * the index for inputing
     */

    private int getlog;
    T prior;

    /**
     * Constructior that takes param of array with appropriate type
     * @param aRef
     */
    PriorityQueue(T[] aRef) {
        putlog = 0;
        getlog = 0;
        queue = aRef;
    }

    /**
     * Adding elements to queue
     * @param element
     * @throws QueueFullException
     */
    public void add(T element) throws QueueFullException {

        if (putlog == getlog - 1) {
            throw new QueueFullException(queue.length);
        }
        queue[putlog++] = element;
    }

    /**
     * Also adds element to queue but does not throw an exception
     * @param element
     * @return true if element is added
     */
    public boolean offer(T element) {
        if (putlog == getlog - 1) {
            return false;
        } else {
            queue[putlog++] = element;
            return true;
        }


    }


    /**
     * remove element from queue
     * @return removed element
     * @throws QueueEmptyException
     */
    public T remove() throws QueueEmptyException {
        if (getlog == putlog) {
            throw new QueueEmptyException();
        } else {


            int index =   getPrior();
            T element  =queue[index];

                for( ; index< putlog;index++) {
                    queue[index] = queue[index+1];
                }
                queue[putlog] = null;
                putlog--;
            return element;
        }
    }

    /**
     * also removers element from queue but does not throw exception
     * @return removed element or head of the queue
     */
    public T pool() {

        if (getlog == putlog) {
           return queue[0];
        }
        else {

            int index =   getPrior();
            T element  =queue[index];

            for( ; index< putlog;index++) {
                queue[index] = queue[index+1];
            }
            return element;
        }
    }

    /**
     * finds the most suitable element to remove
     * @return index of suitable element
     */
    private int getPrior(){
        prior = queue[0];
        int index = 0;

        //for (T e : queue)

        for(int i =1; i<putlog; i++) {
            if (queue[i].compareTo(prior) < 0) {
                prior = queue[i];
                index = i;
            }

        }
        return index;
    }


    public boolean isEmpty() {
        if(getlog == putlog)
            return true;
        else
            return false;
    }
}