package com.priority_queue;

public class QueueEmptyException extends Exception {

    public String what() {
        return "Queue is empty!\n";
    }
}
