package com.priority_queue;

public class QueueFullException extends Exception {

    int size;
    QueueFullException(int size) {
        this.size = size;
    }

    public String what() {
        return "The size is over " + size +"\n";
    }
}
