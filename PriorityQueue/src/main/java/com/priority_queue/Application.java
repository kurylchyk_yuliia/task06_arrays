package com.priority_queue;
import org.apache.logging.log4j.*;
public class Application {

    private static Logger logger1 = LogManager.getLogger(Application.class);
    public static void main(String[] args) {
        Integer  iStore[]  =  new  Integer[10];
        PriorityQueue<Integer>  q  =  new PriorityQueue<Integer>(iStore);

        try {
            q.add(5);
            q.add(10);
            q.add(17);
            q.add(4);
            q.add(3);
            q.add(2);
          while(!q.isEmpty()) {
              logger1.info(q.remove());
          }

        } catch(QueueEmptyException ex) {
            logger1.fatal(ex.what());
        } catch(QueueFullException ex) {
            logger1.fatal(ex.what());
        }

    }
}
